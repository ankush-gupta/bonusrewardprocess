package com.coolboots;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.coolboots.service.BonusRewardProcessService;

@SpringBootApplication
@ComponentScan("com.coolboots")
public class BonusRewardProcessApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BonusRewardProcessApplication.class, args);
	}
	
	@Autowired
	BonusRewardProcessService processService;
	
	@Override
	public void run(String... args) throws Exception {
		processService.ExecuteProcess();
	}

}
