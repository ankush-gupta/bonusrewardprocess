package com.coolboots.dao;

import com.coolboots.dto.CashTransactionDto;
import com.coolboots.dto.PlayerDetailsDto;

public interface ProcessDao {

	boolean UpdateDataIntoCashWallet(PlayerDetailsDto playerDetailsDto);

	void InsertDataIntoCashTransaction(CashTransactionDto cashTransactionDto);

}
