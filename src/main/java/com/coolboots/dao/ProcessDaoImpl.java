package com.coolboots.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.coolboots.dto.CashTransactionDto;
import com.coolboots.dto.PlayerDetailsDto;
import com.coolboots.service.BonusRewardProcessService;

@Repository
public class ProcessDaoImpl implements ProcessDao {
	private static final Logger logs = LoggerFactory.getLogger(ProcessDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public boolean UpdateDataIntoCashWallet(PlayerDetailsDto playerDetailsDto) {
		try {
			String sql = "UPDATE `cash_wallet` SET `paytm_deposit`=`paytm_deposit`+ ?, `modified`= CURRENT_TIMESTAMP WHERE user_id = ?";
			return jdbcTemplate.update(sql, new Object[] { playerDetailsDto.getAmount(), playerDetailsDto.getUid()}) > 0;
		} catch (Exception e) {
			logs.error("Exception in ProcessDaoImpl UpdateDataIntoCashWallet() : {}", e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void InsertDataIntoCashTransaction(CashTransactionDto cashTransactionDto) {
		try {
			if(cashTransactionDto != null) {
				mongoTemplate.insert(cashTransactionDto, "cash_transactions");
			}
		} catch (Exception e) {
			logs.error("Exception in ProcessDaoImpl InsertDataIntoCashTransaction() : {}", e.getMessage());
			e.printStackTrace();
		}
	}	
}
