package com.coolboots.dto;

public class CashTransactionDto {
	private long user_id;
	private long amount;
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return String.format("CashTransactionDto [user_id=%s, amount=%s]", user_id, amount);
	}
}
