package com.coolboots.dto;

public class PlayerDetailsDto {
	private long uid;
	private int mobile_no;
	private long amount;
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public int getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(int mobile_no) {
		this.mobile_no = mobile_no;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return String.format("PlayerDetailsDto [uid=%s, mobile_no=%s, amount=%s]", uid, mobile_no, amount);
	}
	
}
