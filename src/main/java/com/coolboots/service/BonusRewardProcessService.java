package com.coolboots.service;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.dao.ProcessDao;
import com.coolboots.dto.CashTransactionDto;
import com.coolboots.dto.PlayerDetailsDto;
import com.opencsv.CSVReader;

@Service
public class BonusRewardProcessService {

	@Autowired
	ProcessDao processDao;
	
	private static final Logger logs = LoggerFactory.getLogger(BonusRewardProcessService.class);
	
	public void ExecuteProcess() {
		try {
			
			String csvFile = "/var/www/html/Bonus_Reward_Players/test.csv";
			List<PlayerDetailsDto> csvDataList = null;
			CashTransactionDto cashTransactionDto;
			
			//Read csv file 
			csvDataList = readCsvFile(csvFile);
			
			if(csvDataList != null && csvDataList.size() > 0) {
				for (PlayerDetailsDto playerDetailsDto : csvDataList) {
					//update in mysql cash_wallet
					boolean status = processDao.UpdateDataIntoCashWallet(playerDetailsDto);
					if(status == true) {
						//insert into mongo
						cashTransactionDto = new CashTransactionDto();
						cashTransactionDto.setUser_id(playerDetailsDto.getUid());
						cashTransactionDto.setAmount(playerDetailsDto.getAmount());
						
						processDao.InsertDataIntoCashTransaction(cashTransactionDto);
					}
					
				}
			}
			
		} catch (Exception e) {
			logs.error("Exception in BonusRewardProcessService ExecuteProcess() : {}", e.getMessage());
			e.printStackTrace();		
		}
	}
	
	
	
	private List<PlayerDetailsDto> readCsvFile(String csvfile) {
		PlayerDetailsDto dto;
		List<PlayerDetailsDto> dtoList = null;
	    CSVReader reader = null;
	    try {
	    	dtoList = new ArrayList<PlayerDetailsDto>();
	        reader = new CSVReader(new FileReader(csvfile));
	        String[] line;
	        reader.readNext();
	        while ((line = reader.readNext()) != null) {
	        	 System.out.println("Country [id= " + line[0] + ", code= " + line[1] + " , name=" + line[2] + "]");
	        	dto = new PlayerDetailsDto();
	        	dto.setUid(Long.parseLong(line[0].toString()));
	        	dto.setMobile_no(Integer.parseInt(line[1].toString()));
	        	dto.setAmount(Long.parseLong(line[2].toString()));
	        	
	        	dtoList.add(dto);
	        }
	    } catch (IOException e) {
	    	logs.error("Exception in BonusRewardProcessService readCsvFile() : {}", e.getMessage());
	        e.printStackTrace();
	    }
	    return dtoList;
	}

}
